"use strict";
/*
1. Об'єкти в JS мають спеціальну приховану властивість [[Prototype]], яке або дорівнює null, або посилається на інший об'єкт. Цей об'єкт називається "прототип". Якщо об'єкт B є прототипом об'єкта A, то коли у B є властивість, A успадкує ту саму властивість, якщо інше не вказано явно. B, своєю чергою, може успадковуватися від C і т.д. Це називається ланцюжком прототипів. Ланцюжок завершується об'єктом, у якого властивість prototype дорівнює null.
2. super() використовується як функція, що викликає батьківський конструктор та отримує доступ до батьківських властивостей і методів.
*/

class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this._salary = salary;
        Employee.counter +=1;
    }
    get salary(){
        return this._salary;
    }
    set salary(newVal){
        this._salary = newVal;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }
    get salary(){
        return super.salary * 3;
    }
}

Employee.counter = 0;

const dev1 = new Programmer('Pete', 25, 2500, 'JavaScript, PHP')
console.log(dev1);
console.log(dev1.salary);

const dev2 = new Programmer('Michele', 30, 3000, 'Python, Perl')
console.log(dev2);
console.log(dev2.salary);

const dev3 = new Programmer('Steve', 35, 3500, 'Java, C++')
console.log(dev3);
console.log(dev3.salary);

console.log(`----------------
The total number of employees: ${Employee.counter}`)